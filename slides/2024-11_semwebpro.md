---
title: Formaliser la demande et l'autorisation d'accès dans l'écosystème Solid
permalink: "slides/2024/semwebpro/"
---

<div class="title-slide">

<div>

# Formaliser la demande et l'autorisation d'accès dans l'écosystème Solid

Dr. Nicolas Ayral Seydoux, ingénieur chez Inrupt

  <div class="logo-banner">
    <img src="/slides/assets/logo-inrupt.svg"></img>
    <img src="/slides/assets/logo-solid.svg"></img>
  </div>

</div>

</div>

---

<!-- Clair: 01bfe0 -->
<!-- Foncé: 814aff -->
## Séparer applications et données avec Solid

<!-- Composants de Solid:
- L'utilisateur
- Le stockage (données, métadonnées, ...)
- L'application
- Le fournisseur d'identité

voir http://mermaid.js.org/syntax/architecture.html -->

<div class="diagram">
  <object data="/slides/assets/solid-core-concepts.svg"></object>
</div>

---

<!--
## Couches de standards Solid

- HTTP
- RDF
- LDP (ish)
- OIDC + WAC/ACP (contrôle d'accès)

Contrôle d'accès basé sur l'identité

---
-->

## Prouver son identité avec Solid-OIDC

<!-- - Extension de OpenID
- Protocole délégation : l'agent agit au nom de l'utilisateur
- Permet l'obtention d'un jeton d'identité
- Chaîne de confiance: WebID -> Fournisseur d'identité, Stockage -> Fournisseur d'identité
- L'application ne connaît pas les identifiants -->


<div class="diagram">
  <object data="/slides/assets/user-identification.svg"></object>
</div>

---

## Cas d'utilisation Solid-OIDC


<div class="diagram">
  <object data="/diagrams/quadrants.svg"></object>
</div>

---

## Autorisation de la requête authentifiée

<!-- - Une fois qu'on a obtenu un jeton d'accès, le serveur vérifie si l'accès est autorisé
- Chaque ressource a des méta-resources "associées"
- ACP définit une resource pour le contrôle d'accès -->

<div class="two-columns">
<div>

- **Le Pod** autorise les requêtes
- Chaque resource a des **"ressources associées"**
- **ACP** définit une resource pour le contrôle d'accès

</div>
<div class="diagram">
  <object data="/slides/assets/acr.svg"></object>
</div>
</div>

---

## ACP, base et points d'extension

<!-- - La resource contient des politiques d'accès
- Chaque politique définit des droits d'accès et des conditions dans lesquelles ils s'appliquent
- La specification définit les conditions comme point d'extension -->

<!-- ```
ex:someAcr
  acp:resource ex:someResource ;  
  acp:accessControl [
    acp:apply ex:aliceWrite ;
  ] .

  ex:aliceWrite
    acp:allow acl:Read, acl:Write ;
    acp:allOf ex:aliceMatcher .

  ex:aliceMatcher
    a acp:Matcher ;
    acp:agent ex:Alice .
``` -->

<div class="diagram">
  <object data="/slides/assets/acr-content.svg"></object>
</div>

---

## Quelques limites pour ACP

- Un client autorisé à modifier les ACR est **très privilégié**
- ACP définit un **modèle de données**, mais pas de **protocole**

<!-- Un approche complémentaire est nécessaire pour permettre le partage facilement -->

---

## Cas d'utilisation à adresser

- Flux entre un **service** et un **utilisateur**
- Utilisateur et Agent autonome du service **authentifiés avec Solid-OIDC**
- L'agent veut agir **en son nom propre** <!-- (pas au nom de l'utilisateur comme dans Solid-OIDC normal) -->
- Hypothèses: l'agent sait de quelles ressource il a besoin

---

## Access Requests/Access Grants

- Représentations d'une **demande d'accès et de sa réponse**
- Implémenté à partir de **Verifiable Credentials** (VC)
- **Immuable**, **borné dans le temps**, **vérifiable**, **révocable**
- Extensible selon les cas d'utilisation
- Nécessite **confiance** du Pod au fournisseur VC 

---

## Intégrer les Access Grants à ACP

<div class="two-columns">
<div>

- Introduction d'un **matcher** idoine
- Instancié à la **racine de la hierarchie**
- Mécanismes natifs ACP pour **bloquer** le matcher

</div>
<div class="diagram">
  <object data="/slides/assets//acr-ag.svg"></object>
</div>
</div>




---

## Détails d'une Access Grant

<!-- {
  "id": "https://vc.inrupt.com/vc/3ebd5ad2-07bb-472b-8070-0a77fa67375d",
  "type": [
    "SolidAccessGrant",
    "VerifiableCredential"
  ],
  "proof": {
    "type": "Ed25519Signature2020",
    "created": "2024-11-16T21:58:15.987Z",
    "domain": "solid",
    "proofPurpose": "assertionMethod",
    "proofValue": "z3JgTD51BZLroaaMaoP3LivnEtTXUJWGEouEhALrNDizsGiGo8n7RGBBxu6c2nAQDZzn8LQTjNqocxPCWjzpmrFPB",
    "verificationMethod": "https://vc.inrupt.com/key/286f4df0-c330-3783-b745-3ed5afc8c560"
  },
  "credentialStatus": {
    "id": "https://vc.inrupt.com/status/2Y6Z#0",
    "type": "RevocationList2020Status",
    "revocationListCredential": "https://vc.inrupt.com/status/2Y6Z",
    "revocationListIndex": "0"
  },
  "credentialSubject": {
    "id": "https://id.inrupt.com/zwifi",
    "providedConsent": {
      "mode": [
        "Read"
      ],
      "forPersonalData": [
        "https://storage.inrupt.com/5230bffd-1fa3-4dc1-bd63-b624a7a2b1d9/39abe778-56d3-4c9b-ab5f-c0c4a7ba7c4d.txt"
      ],
      "hasStatus": "ConsentStatusExplicitlyGiven",
      "isProvidedTo": "https://id.inrupt.com/zwifi"
    }
  },
  "expirationDate": "2025-11-16T21:57:42.258Z",
  "issuanceDate": "2024-11-16T21:57:42.259Z",
  "issuer": "https://vc.inrupt.com",
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://schema.inrupt.com/credentials/v1.jsonld",
    "https://w3id.org/security/data-integrity/v1",
    "https://w3id.org/vc-revocation-list-2020/v1",
    "https://w3id.org/vc/status-list/2021/v1",
    "https://w3id.org/security/suites/ed25519-2020/v1"
  ]
} -->
<!-- 
- Explicitement lié à l'agent qui demande et à l'agent qui approuve
- Liste les ressources et les droits d'accès (retour d'expérience: récursif) -->

<div class="diagram">
  <object data="/slides/assets/access-grant-content.svg"></object>
</div>

---

## Demander et approuver l'accès à une ressource

<div class="diagram">
  <object data="/diagrams/access-request-flow.svg"></object>
</div>

---

## Une UX de gestion d'accès avec le Data Wallet

- Le <a href="https://github.com/inrupt/inrupt-data-wallet">Inrupt Data Wallet</a> est une **application Solid**
- Scan de QR code pour demande d'accès
- Visualisation des partages en cours 

---

## Les points clés à retenir

- **Contrôle d'accès** basé sur l'**identité** dans Solid
- **ACR combiné avec Access Grants** pour un protocole de partage
- Le Data Wallet comme **couche d'affordance** sur les données

Contact: [`nseydoux@inrupt.com`](mailto:nseydoux@inrupt.com)

<div>
  <div class="logo-banner">
    <img src="/slides/assets/logo-inrupt.svg"></img>
    <img src="/slides/assets/logo-solid.svg"></img>
  </div>
</div>

---


## Solid-OIDC pour un utilisateur via le navigateur

<div class="diagram">
  <object data="/diagrams/auth-code-flow.svg"></object>
</div>

---
## Solid-OIDC pour un agent "autonome"

<div class="diagram">
  <object data="/diagrams/client-credential-flow.svg"></object>
</div>

---

## Annexes: Utiliser une Access Grant pour accéder à une ressource

- UMA à la rescousse
