import { run } from "@mermaid-js/mermaid-cli";
// import { registerIconPacks } from "mermaid"
import { readdir, writeFile } from "node:fs/promises";
import { join, basename, extname } from "path";

// registerIconPacks([
//   {
//     name: icons.prefix, // To use the prefix defined in the icon pack
//     icons,
//   },
// ]);

if (process.env.CI === "true") {
    process.exit(0);
}

async function generateDiagrams(sourceDir, targetDir) {
    try {
        const files = await readdir(sourceDir);
        await Promise.all(files.map(async (path) => {
            const srcPath = join(sourceDir, path);
            const targetPath = join(
                targetDir,
                basename(path).replace(extname(path), ".svg")
            );
            await writeFile(targetPath, "");
            console.log(`Converting ${srcPath} to ${targetPath}`);
            return run(srcPath, targetPath, {
                outputFormat: "svg",
                parseMMDOptions: {
                    mermaidConfig: {
                        sequence: {
                            mirrorActors: false
                        }
                    }
                }
            });
        }));
      } catch (err) {
        console.error(err);
      }
}

const sourceDir = join("diagrams", "source");
const targetDir = join("diagrams", "target");
// Ensure diagrams are generated before processing content.
await generateDiagrams(sourceDir, targetDir);