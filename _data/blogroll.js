const { config } = require("dotenv");

config({ path: ".env.local" });

const UNCATEGORIZED_TAG = "Sans catégorie"

module.exports = async () => {
    const API_KEY = process.env.FRESHRSS_API_KEY;
    if (API_KEY === undefined) {
        throw new Error("process.env.FRESHRSS_API_KEY is undefined.")
    }
    const { feeds } = await fetch("https://rss.zwifi.eu/api/fever.php?api&feeds", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `api_key=${API_KEY}`
    }).then(response => response.json());
    const { groups, feeds_groups: feedGroups } = await fetch("https://rss.zwifi.eu/api/fever.php?api&groups", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `api_key=${API_KEY}`
    }).then(response => response.json());
    
    // Merge the groups and the feeds into an object keyed by groups title containing the feeds details.
    // For instance: 
    // [{
    //   "title": "Blogs",
    //   "feeds": [
    //     {
    //       "title": "Blogs on Drew DeVault's blog",
    //       "url": "https://drewdevault.com/blog/index.xml",
    //       "site_url": "https://drewdevault.com",
    //     },
    //     ...
    return groups.reduce((prev, cur) => {
        const result = { title: cur.title };
        if (cur.title === UNCATEGORIZED_TAG) {
            result.title = "Uncategorized";
        }
        const [currentFeedGroup] = feedGroups.filter((feedGroup) => feedGroup.group_id === cur.id);
        if (currentFeedGroup !== undefined) {
            const currentFeeds = feeds.filter(
                (feed) => currentFeedGroup.feed_ids.split(",").map((str) => Number.parseInt(str)).includes(feed.id)
            );
            result["feeds"] = currentFeeds;
        } else {
            result["feeds"] = []
        }
        return [...prev, result];
    }, []);
};